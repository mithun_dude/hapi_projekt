import anyTest, {TestInterface} from 'ava';
import { Server, ServerApplicationState } from '@hapi/hapi';
/**
 * Represents the Object thats is to be assigned to Execution context.
 
*/
interface App extends ServerApplicationState {
  db: any;
}
interface Context {
  account: any;
  location1:any
  user1: any;
  server: Server
}

export default Context