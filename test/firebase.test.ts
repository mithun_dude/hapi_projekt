import anyTest, { TestInterface } from 'ava';
import Context from './modals/context.modal';
import firebaseHelper from './helpers/firebase.helper';
import serverHelper from './helpers/server.helper';


const test = anyTest as TestInterface<Context>
test.before(async t => {
  try {
    await serverHelper.setup(t);
    await firebaseHelper.setup(t);
  } catch (err) {
    throw err;
  }
})

test.after(async t => {
  try {
    await firebaseHelper.teardown(t);
    await serverHelper.teardown(t);
  } catch (err) {
    throw err;
  }
})

test('Test create/update/remove user', async t => {
  const db = t.context.server.app.db;
  const u = await db.getUserByEmail(t.context.user1.email);
  t.is(u.email, t.context.user1.email);
  t.is(u.emailVerified, true);

  const u2 = await db.getUser(u.uid);
  t.is(u2.uid, u.uid);

  const u3 = await db.updateUser(u.uid, { displayName: 'apa', emailVerified: false, });
  t.is(u3.displayName, 'apa');
  t.is(u3.emailVerified, false);

  await db.removeByEmail(u.email);
  const u4 = await db.getUser(u.uid);
  t.is(u4, null);
});

test('Test fetchOne', async t => {
  const db = t.context.server.app.db;
  const user = await db.fetchOne(`users/${t.context.user1.id}`);
  t.deepEqual(user, t.context.user1);
});

test('Test fetch query', async t => {
  const db = t.context.server.app.db;
  const result = await db.fetch(db.query(`accounts/${t.context.account.id}/locations`).where('shortName', '==', t.context.location1.shortName));
  t.truthy(result);
  t.is(result.length, 1);
  t.deepEqual(result[0], t.context.location1);
});

test('Test update', async t => {
  const db = t.context.server.app.db;
  await db.update(`accounts/${t.context.account.id}`, { businessName: 'Gurka' });
  const account = await db.fetchOne(`accounts/${t.context.account.id}`);
  t.is(account.businessName, 'Gurka');
});

test('Test update with delete', async t => {
  const db = t.context.server.app.db;
  await db.update(`accounts/${t.context.account.id}`, { businessName: undefined });
  const account = await db.fetchOne(`accounts/${t.context.account.id}`);
  t.is(account.businessName, undefined);
});

test('Test set', async t => {
  const db = t.context.server.app.db;
  await db.set(`users/${t.context.user1.id}`, { name: t.context.user1.name });
  const user = await db.fetchOne(`users/${t.context.user1.id}`);
  t.is(Object.keys(user).length, 2);
  t.is(user.id, t.context.user1.id);
  t.is(user.name, t.context.user1.name);
});

test('Test remove', async t => {
  const db = t.context.server.app.db;
  await db.remove(`accounts/${t.context.account.id}`);
  const account = await db.fetchOne(`accounts/${t.context.account.id}`);
  const location1 = await db.fetchOne(`accounts/${t.context.account.id}/locations/${t.context.location1.id}`);
  t.is(account, null);
  t.deepEqual(location1, t.context.location1);
});

test('Test remove with subCol', async t => {
  const db = t.context.server.app.db;
  await db.remove(`accounts/${t.context.account.id}`, true);
  const account = await db.fetchOne(`accounts/${t.context.account.id}`);
  const location1 = await db.fetchOne(`accounts/${t.context.account.id}/locations/${t.context.location1.id}`);
  t.is(account, null);
  t.is(location1, null);
});

test('Test remove collection', async t => {
  const db = t.context.server.app.db;
  await db.removeCol(`accounts/${t.context.account.id}/locations`);
  const result = await db.fetch(db.query(`accounts/${t.context.account.id}/locations`));
  t.is(result.length, 0);
});

test('Test remove collection with subCols', async t => {
  const db = t.context.server.app.db;
  await db.removeCol(`accounts/${t.context.account.id}/visits`, true);
  const visits = await db.fetch(db.query(`accounts/${t.context.account.id}/visits`));
  t.is(visits.length, 0);
});

test('Test locks', async t => {
  const db = t.context.server.app.db;
  let [lock1, lock2, lock3, lock4, lock5] = await Promise.all([
    db.tryLock(`locks/${t.context.account.id}`, 1000),
    db.tryLock(`locks/${t.context.account.id}`, 1000),
    db.tryLock(`locks/${t.context.account.id}`, 1000),
    db.tryLock(`locks/${t.context.account.id}`, 1000),
    db.tryLock(`locks/${t.context.account.id}`, 1000),
  ]);
  t.is(lock1 + lock2 + lock3 + lock4 + lock5, 1);
  let [lock6, lock7] = await Promise.all([
    db.tryLock(`locks/${t.context.account.id}`, 1000),
    db.tryLock(`locks/${t.context.account.id}`, 1000),
  ]);
  t.is(lock6, false);
  t.is(lock7, false);

  await db.releaseLock(`locks/${t.context.account.id}`);
  const lock8 = await db.tryLock(`locks/${t.context.account.id}`);
  t.is(lock8, true);
  await db.releaseLock(`locks/${t.context.account.id}`);
});