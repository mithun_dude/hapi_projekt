// see test-result.log for the test  info.

import anyTest, {TestInterface} from 'ava';
import  serverHelper from  './helpers/server.helper'
import Context from './modals/context.modal';

// import init from '../server/app';



const test = anyTest as TestInterface<Context>

test.before(async (t) => {
  try{
    await serverHelper.setup(t)
    // t.context.server = await init(false)  
  }
  catch(err){
    throw err
  }
})

test.after(async (t) => {
  try{
    await serverHelper.teardown(t)
    // delete t.context.server
  }  
  catch(err){
    throw err
  }

})

test('Get /', async (t) => {
  const resp = await t.context.server.inject({
    method: 'GET',
    url: '/'
  });
  t.is(resp.statusCode, 200);
  t.deepEqual(resp.result, { statusCode: 200, message: 'Ok' })
})

test('Get /readiness_check', async t => {
  const response = await t.context.server.inject({
    method: 'GET',
    url: '/readiness_check',
  });
  t.is(response.statusCode, 200);
  t.deepEqual(response.result, {statusCode: 200, message: 'Ok'});
});

test('Get /liveness_check', async t => {
  const response = await t.context.server.inject({
    method: 'GET',
    url: '/liveness_check',
  });
  t.is(response.statusCode, 200);
  t.deepEqual(response.result, {statusCode: 200, message: 'Ok'});
});
