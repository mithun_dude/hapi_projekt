import { ExecutionContext } from 'ava';
import Context from '../modals/context.modal';


async function setup(t: ExecutionContext<Context>) {
  const server = t.context.server;
  // setup firebase users and firestore data needed for tests here
}

async function teardown(t: ExecutionContext<Context>) {
  // clean up firebase users and firestore data needed for tests here
}

export default {setup,teardown}