import init from '../../server/app'

import { ExecutionContext } from 'ava';
import Context from '../modals/context.modal';

 async function setup(t:ExecutionContext<Context>){
  t.context.server = await init(false);
}

 async function teardown(t:ExecutionContext<Context>){
  delete t.context.server
}

export default {setup,teardown}