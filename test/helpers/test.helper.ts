
// thsi file is not found
// const stringUtil = require('../../lib/string-util');

function randomPhone() {
  // return '+1600555' + stringUtil.randomChars(4, '0123456789');
}

function randomEmail() {
  return 'test.' + stringUtil.randomChars(16, 'abcdefghijklmnopqrstuvwxyz0123456789') + '@waitwhile.com';
}

let _DateNow:number

function patchDate(timestamp) {
  _DateNow = global.Date.now;
  global.Date.now = () => timestamp;
}

function unpatchDate() {
  global.Date.now = _DateNow;
}

module.exports = {
  randomPhone,
  randomEmail,
  patchDate,
  unpatchDate,
};