'use strict';

import test from 'ava'

import util from '../server/lib/util';

test('Test binarySearch', async t => {
  const arr1 = [0, 5, 7, 8, 9, 20];
  t.is(util.binarySearch(arr1, 0), 0);
  t.is(util.binarySearch(arr1, 7), 2);
  t.is(util.binarySearch(arr1, 20), 5);
  t.is(util.binarySearch(arr1, -1), -1);
  t.is(util.binarySearch(arr1, 4), -2);
  t.is(util.binarySearch(arr1, 25), -7);

  const arr2 = [{ id: 5, value: 0 }, { id: 5, value: 1 }, { id: 6, value: 2 }];
  t.is(util.binarySearch(arr2, arr2[0], (a:any, b:any) => (a.id - b.id) || (a.value - b.value)), 0);
  t.is(util.binarySearch(arr2, arr2[1], (a:any, b:any) => (a.id - b.id) || (a.value - b.value)), 1);
  t.is(util.binarySearch(arr2, arr2[2], (a:any, b:any) => (a.id - b.id) || (a.value - b.value)), 2);
});
