import {Server} from '@hapi/hapi'
import * as dotenv from 'dotenv'
import initPlugins from './plugins'
import initRoutes from './routes'


const path  = __dirname+'/.env';
// const path = __dirname+'/.prod.env'

// dotenv.config()

const server = new Server({
port:8888,
host:'localhost',
routes:{cors:true}
})

async function start(listen:boolean){
  try{
  
    //init the plugins and the routes
    await initPlugins(server);
    initRoutes(server);

    if(listen){      
      await server.start()
    } 
    else{
      server.initialize()
      console.log('Server Initialized')
    }
    return server
  }
  catch(err){
    throw err
  }
  
}

export default start  