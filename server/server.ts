import start from './app'

async function server(){
  try{
    const server = await start(true);
    console.log('server started@:',server.info.uri)
  }
  catch(err){
    throw err
  }  
}

server();