import {Plugin} from '../modals/plugin.interface'
import { Server } from '@hapi/hapi'

const packagepath = '/home/user/projects/typescript/hapi_ts/package.json'

async function pluginFunction(server:Server,options:any){
    server.route({
      method:['GET','POST'],
      path:'/plugin',
      handler:function(request,handler){
        return handler.response({msg:options.msg}).code(200)
      }
    })
}

export const plugin:Plugin ={
    pkg:require(packagepath),
    register:pluginFunction       
}

