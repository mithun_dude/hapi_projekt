import { ServerRegisterPluginObject, Server } from "@hapi/hapi";

import log from '../lib/logging';
import db from '../lib/db'

const logging: ServerRegisterPluginObject<{ignorePaths:string[]}> = {
  plugin: log,
  options: {
    ignorePaths: ['/readiness_check', '/liveness_check'],
  }

}

//TODO
const dbinit:ServerRegisterPluginObject<any> = {
  plugin:db,
  options:{
    firebaseProjectId: process.env.FIREBASE_PROJECT_ID,
            firebaseClientEmail: process.env.FIREBASE_CLIENT_EMAIL,
            firebasePrivateKey: process.env.FIREBASE_PRIVATE_KEY,
            firebaseUrl: process.env.FIREBASE_URL,
            cache: {
                cache: 'default',
                expiresIn: 60 * 1000,
            },
  }
}

async function initPlugins(server: Server) {
  try {
    await server.register(logging);
    // await server.register(dbinit);    
  }
  catch (err) {
    throw err
  }
}

export default initPlugins