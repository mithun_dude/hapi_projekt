'use strict';

function binarySearch(array, value, compareFunc) {
  compareFunc = compareFunc || ((a, b) => a - b);
  let m = 0;
  let n = array.length - 1;
  while (m <= n) {
    let k = (n + m) >> 1;
    let cmp = compareFunc(value, array[k]);
    if (cmp > 0) {
      m = k + 1;
    } else if (cmp < 0) {
      n = k - 1;
    } else {
      return k;
    }
  }
  return -m - 1;
}

function getExpBackoff(numRetry, factor, base) {
  factor = factor || 500;
  base = base || 2;
  return Math.floor(Math.pow(base, numRetry) * factor);
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function sleepExpBackoff(numRetry, factor, base) {
  return sleep(getExpBackoff(numRetry, factor, base));
}

function getRequestIp(request) {
  let ip = request.headers['x-appengine-user-ip'];
  if (!ip) {
    ip = request.headers['x-forwarded-for'];
    if (ip) {
      const sep = ip.indexOf(',');
      if (sep !== -1) {
        ip = ip.substring(0, sep);
      }
    } else {
      ip = request.info.remoteAddress;
    }
  }
  return ip;
}

module.exports = {
  binarySearch,
  sleepExpBackoff,
  getRequestIp,
};
