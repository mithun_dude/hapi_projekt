'use strict';

const Hoek = require('@hapi/hoek');
const logger = require('./logger');
const util = require('../util2');

const noopLogger = {
  trace: () => {},
  debug: () => {},
  info: () => {},
  warn: () => {},
  error: () => {},
  fatal: () => {},
};

module.exports = {
  name: 'logging',
  register: async function(server, options) {
    const ignoreTable = {};
    if (options.ignorePaths) {
      for (let i = 0; i < options.ignorePaths.length; i++) {
        ignoreTable[options.ignorePaths[i]] = true;
      }
    }

    server.decorate('server', 'logger', () => logger);

    server.ext('onRequest', (request, h) => {
      if (ignoreTable[request.path]) {
        request.logger = noopLogger;
        return h.continue;
      }
      const headers = request.headers && Hoek.clone(request.headers) || {};
      delete headers.authorization;
      delete headers.accesstoken;

      request.logger = logger.child({
        req: {
          id: request.info.id,
          method: request.method,
          url: request.url.pathname + request.url.search,
          headers,
          remoteIp: util.getRequestIp(request),
        },
        res: null,
        responseTime: null,
        userId: null,
        accountId: null,
      });
      return h.continue;
    });

    server.ext('onPostAuth', (request, h) => {
      if (ignoreTable[request.path]) {
        return h.continue;
      }
      const user = request.auth.credentials;
      // TODO: Enable request payload logging when we have fixed issue with too large payloads
      //const req = request.logger.bindings().req || {};
      //req.payload = request.payload;
      request.logger = request.logger.child({
        //req,
        userId: user && user.id || null,
        accountId: user && user.accountId || null,
      });
      return h.continue;
    });

    server.events.on('log', (event) => {
      if (event.error) {
        logger.error({ err: event.error }, event.error.stack);
      } else {
        logger.info(event.data);
      }
    });

    server.events.on('request', (request, event, tags) => {
      if (ignoreTable[request.path] || event.channel === 'internal') {
        return;
      }
      if (event.error) {
        request.logger.error({ err: event.error, tags }, event.error.stack);
      } else if (event.channel === 'app') {
        request.logger.info(event.data);
      }
    });

    server.events.on('response', (request) => {
      if (ignoreTable[request.path]) {
        return;
      }
      const info = request.info;
      const responseTime = info.responded - info.received;
      const response = request.response || {};

      let message = request.method.toUpperCase() || '-';
      message += ' ';
      message += request.url.pathname;
      message += request.url.search;
      message += ' ';
      message += response.statusCode || '-';
      message += ' ';
      message += response.headers && response.headers['content-length'] || '-';
      message += 'b ';
      message += responseTime;
      message += 'ms';

      const meta = {
        res: {
          statusCode: response.statusCode,
          headers: response.headers || {}
        },
        responseTime,
      };
      if (response._error) {
        meta.err = response._error;
        request.logger.warn(meta, message);
      } else {
        request.logger.info(meta, message);
      }
    });
  },
};