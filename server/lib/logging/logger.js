'use strict';

const pino = require('pino');

const logger = pino({
  level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
  customLevels: { audit: 0 },
}, pino.destination(process.env.NODE_ENV === 'test' ? 2 : 1));

module.exports = logger;