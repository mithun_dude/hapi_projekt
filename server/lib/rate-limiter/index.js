'use strict';

const RedisRateLimiter = require('./redis-rate-limiter');
const MemoryRateLimiter = require('./memory-rate-limiter');

module.exports = {
  name: 'rate-limiter',
  register: async function(server, options) {
    const enabled = options.enabled !== false;


    /**
     * Cut early if rate limiting is disabled.
     */
    if (!enabled) {
      return;
    }

    /**
     * This property stores the request’s rate limit
     * details for the response headers.
     */
    server.decorate('request', 'rateLimit');

    const RateLimiter = ((options.host && options.port) || options.client || options.url || options.socket) ? RedisRateLimiter : MemoryRateLimiter;
    const limiter = new RateLimiter(server, options);

    /**
     * Start the rate limiter before before the server.
     */
    server.ext('onPreStart', async () => {
      await limiter.start();
    });

    /**
     * Rate limit incoming requests.
     */
    server.ext('onPostAuth', async (request, h) => {
      return limiter.handle(request, h);
    });

    /**
     * Append rate-limit related headers to each
     * response, also to an error response.
     */
    server.ext('onPreResponse', (request, h) => {
      return limiter.addHeaders(request, h);
    });

    /**
     * Shut down rate limiter after server stop.
     */
    server.ext('onPostStop', async () => {
      await limiter.stop();
    });
  },
};