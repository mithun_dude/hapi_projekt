'use strict';

const Boom = require('@hapi/boom');
const Redis = require('ioredis');

const util = require('../util');

const defaults = {
  max: 300,
  duration: 60 * 1000,
  userIdAttribute: 'id',
  userRateLimitMaxAttribute: 'rateLimitMax',
  userRateLimitDurationAttribute: 'rateLimitDuration',
  skip: () => false,
  host: '127.0.0.1',
  port: 6379,
  namespace: 'rate-limit',
  lazyConnect: false,
  reconnectOnError: err => {
    if (err && err.message && err.message.startsWith('READONLY')) {
      return true; // Only reconnect when the error starts with "READONLY"
    }
  },
  retryStrategy: times => Math.min(100 * Math.pow(2, times), 30000),
};

class RedisRateLimiter {
  constructor(server, options = {}) {
    this.server = server;
    this.options = Object.assign({}, defaults, options);

    this.shouldSkip = this.options.skip;
    this.userIdAttribute = this.options.userIdAttribute;
    this.userRateLimitMaxAttribute = this.options.userRateLimitMaxAttribute;
    this.userRateLimitDurationAttribute = this.options.userRateLimitDurationAttribute;
    this.namespace = this.options.namespace;
    this.max = this.options.max;
    this.duration = this.options.duration;
  }

  async start() {
    if (this.options.client) {
      this.client = this.options.client;
    }
    if (this.client) {
      return;
    }
    if (this.options.url) {
      this.client = new Redis(this.options.url, this.options);
    } else if (this.options.socket) {
      this.client = new Redis(this.options.socket, this.options);
    } else {
      this.client = new Redis(this.options);
    }
  }

  async stop() {
    try {
      if (this.client && !this.options.client) {
        await this.client.disconnect();
      }
    } finally {
      this.client = null;
    }
  }

  async handle(request, h) {
    if (this.shouldSkip(request)) {
      return h.continue;
    }
    let id;
    let max = this.max;
    let duration = this.duration;
    if (this.userIdAttribute && request.auth && request.auth.credentials) {
      id = request.auth.credentials[this.userIdAttribute];
      max = request.auth.credentials[this.userRateLimitMaxAttribute] || this.max;
      duration = request.auth.credentials[this.userRateLimitDurationAttribute] || this.duration;
    }
    if (!id) {
      id = util.getRequestIp(request);
      if (!id) {
        return h.continue;
      }
    }
    request.rateLimit = await this.getRateLimit(id, max, duration);
    if (request.rateLimit.remaining) {
      return h.continue;
    }
    throw Boom.tooManyRequests('You have exceeded the request limit');
  }

  async getRateLimit(id, max, duration) {
    const key = `${this.namespace}:${id}`;
    const now = Date.now();
    const start = now - duration;

    const operations = [
      ['zremrangebyscore', key, 0, start],
      ['zcard', key],
      ['zadd', key, now, now],
      ['zrange', key, 0, 0],
      ['zrange', key, -max, -max],
      ['pexpire', key, duration],
    ];
    try {
      if (!this.client || this.client.status !== 'ready') {
        throw new Error('Disconnected');
      }
      const result = await this.client.multi(operations).exec();
      const count = result[1][1];
      const oldest = parseInt(result[3][1][0], 10);
      const oldestInRange = parseInt(result[4][1][0], 10);
      return {
        remaining: count < max ? max - count : 0,
        reset: (Number.isNaN(oldestInRange) ? oldest : oldestInRange) + duration,
        total: max,
      };
    } catch (err) {
      this.server.logger().warn({ err }, 'Rate limiting failed with %s', err);
      return {
        remaining: max,
        reset: now,
        total: max,
      };
    }
  }

  addHeaders(request, h) {
    if (!request.rateLimit) {
      return h.continue;
    }
    const { total, remaining, reset } = request.rateLimit;
    const response = request.response;
    if (response.isBoom) {
      response.output.headers['X-Rate-Limit-Limit'] = total;
      response.output.headers['X-Rate-Limit-Remaining'] = Math.max(0, remaining - 1);
      response.output.headers['X-Rate-Limit-Reset'] = reset;
      return response;
    }
    return response.header('X-Rate-Limit-Limit', total).header('X-Rate-Limit-Remaining', Math.max(0, remaining - 1)).header('X-Rate-Limit-Reset', reset);
  }
}

module.exports = RedisRateLimiter;