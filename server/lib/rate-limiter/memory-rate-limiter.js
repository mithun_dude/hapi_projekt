'use strict';

const Boom = require('@hapi/boom');

const SortedSet = require('../sorted-set');
const util = require('../util');

const defaults = {
  max: 300,
  duration: 60 * 1000,
  userIdAttribute: 'id',
  userRateLimitMaxAttribute: 'rateLimitMax',
  userRateLimitDurationAttribute: 'rateLimitDuration',
  skip: () => false,
};

class MemoryRateLimiter {
  constructor(server, options = {}) {
    this.server = server;
    this.options = Object.assign({}, defaults, options);

    this.shouldSkip = this.options.skip;
    this.userIdAttribute = this.options.userIdAttribute;
    this.userRateLimitMaxAttribute = this.options.userRateLimitMaxAttribute;
    this.userRateLimitDurationAttribute = this.options.userRateLimitDurationAttribute;
    this.namespace = this.options.namespace;
    this.max = this.options.max;
    this.duration = this.options.duration;
  }

  async start() {
    if (this.cache) {
      return;
    }
    this.cache = new Map();
    this.cleanupInterval = setInterval(function(cache) {
      const now = Date.now();
      for (const [key, envelope] of cache) {
        if (now > envelope.ttl) {
          cache.delete(key);
        }
      }
    }, 60000, this.cache);
    this.cleanupInterval.unref();
  }

  async stop() {
    this.cache = null;
    this.cleanupInterval = null;
  }

  async handle(request, h) {
    if (this.shouldSkip(request)) {
      return h.continue;
    }
    let id;
    let max = this.max;
    let duration = this.duration;
    if (this.userIdAttribute && request.auth && request.auth.credentials) {
      id = request.auth.credentials[this.userIdAttribute];
      max = request.auth.credentials[this.userRateLimitMaxAttribute] || this.max;
      duration = request.auth.credentials[this.userRateLimitDurationAttribute] || this.duration;
    }
    if (!id) {
      id = util.getRequestIp(request);
      if (!id) {
        return h.continue;
      }
    }
    request.rateLimit = await this.getRateLimit(id, max, duration);
    if (request.rateLimit.remaining) {
      return h.continue;
    }
    throw Boom.tooManyRequests('You have exceeded the request limit');
  }

  async getRateLimit(id, max, duration) {
    const now = Date.now();
    const start = now - duration;

    const envelope = this.cache.get(id);
    let set;
    if (!envelope) {
      set = new SortedSet();
      this.cache.set(id, { set, ttl: now + duration });
    } else {
      set = envelope.set;
    }
    set.removeRangeByScore(0, start);
    const count = set.size();
    set.add(now, now);
    const oldest = set.range(0, 0)[0];
    const oldestInRange = set.range(-max, -max)[0];
    return {
      remaining: count < max ? max - count : 0,
      reset: (!oldestInRange ? oldest : oldestInRange) + duration,
      total: max,
    };
  }

  addHeaders(request, h) {
    if (!request.rateLimit) {
      return h.continue;
    }
    const { total, remaining, reset } = request.rateLimit;
    const response = request.response;
    if (response.isBoom) {
      response.output.headers['X-Rate-Limit-Limit'] = total;
      response.output.headers['X-Rate-Limit-Remaining'] = Math.max(0, remaining - 1);
      response.output.headers['X-Rate-Limit-Reset'] = reset;
      return response;
    }
    return response.header('X-Rate-Limit-Limit', total).header('X-Rate-Limit-Remaining', Math.max(0, remaining - 1)).header('X-Rate-Limit-Reset', reset);
  }
}

module.exports = MemoryRateLimiter;