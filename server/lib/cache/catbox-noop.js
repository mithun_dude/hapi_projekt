'use strict';

class CatboxNoopCache {

  constructor(options) {
  }

  async start() {
  }

  stop() {
  }

  isReady() {
    return true;
  }

  validateSegmentName(name) {
    return null;
  }

  get(key) {
    return null;
  }

  set(key, value, ttl) {
  }

  drop(key) {
  }
}

module.exports = CatboxNoopCache;