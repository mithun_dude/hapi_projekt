'use strict';

function dateReviver(key, value) {
  if (value && value._seconds) {
    return new Date(value._seconds * 1e3 + Math.round((value._nanoseconds || 0) / 1e6));
  }
  return value;
}

function dateReplacer(key, value) {
  if (this[key] instanceof Date) {
    const ms = this[key].valueOf();
    const s = Math.floor(ms / 1e3);
    return { _seconds: s, _nanoseconds: (ms - s * 1e3) * 1e6 };
  }
  return value;
}

function parseTagAndKey(id, segment, partition) {
  const start = id.indexOf('[');
  const end = start !== -1 ? id.indexOf(']', start + 1) + 1 : -1;
  if (start >= 0 && end >= 2) {
    return { key: `${partition}:data:${segment}:${id.substring(end)}`, tag: `${partition}:tags:${segment}:${id.substring(start, end)}` };
  }
  return { key: `${partition}:data:${segment}:${id}`, tag: null };
}

module.exports = {
  dateReviver,
  dateReplacer,
  parseTagAndKey,
};