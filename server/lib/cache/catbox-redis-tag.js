'use strict';

const Redis = require('ioredis');
const Hoek = require('@hapi/hoek');

const tagCacheUtil = require('./tag-cache-util');

const defaults = {
  host: '127.0.0.1',
  port: 6379,
};

class CatboxRedisTagCache {

  constructor(options) {
    Hoek.assert(this instanceof CatboxRedisTagCache, 'CatboxRedisTagCache must be instantiated using new');

    this.client = null;
    this.options = Object.assign({}, defaults, options, {
      db: options.database || options.db,
      name: options.sentinelName,
      tls: options.tls,
      lazyConnect: false,
      reconnectOnError: err => {
        if (err && err.message && err.message.startsWith('READONLY')) {
          return true; // Only reconnect when the error starts with "READONLY"
        }
      },
      retryStrategy: times => {
        return Math.min(100 * Math.pow(2, times), 30000);
      },
    });
  }

  async start() {
    if (this.options.client) {
      this.client = this.options.client;
    }
    if (this.client) {
      return;
    }
    if (this.options.url) {
      this.client = new Redis(this.options.url, this.options);
    } else if (this.options.socket) {
      this.client = new Redis(this.options.socket, this.options);
    } else {
      this.client = new Redis(this.options);
    }
  }

  async stop() {
    try {
      if (this.client && !this.options.client) {
        this.client.removeAllListeners();
        await this.client.disconnect();
      }
    } finally {
      this.client = null;
    }
  }

  isReady() {
    return !!this.client && this.client.status === 'ready';
  }

  validateSegmentName(name) {
    if (!name) {
      return new Error('Empty string');
    }
    if (name.indexOf('\0') !== -1) {
      return new Error('Includes null character');
    }
    return null;
  }

  async get(idAndSegment) {
    if (!this.client) {
      throw Error('Connection not started');
    }
    const {key} = tagCacheUtil.parseTagAndKey(idAndSegment.id, idAndSegment.segment, this.options.partition);
    //console.error('get', key);

    const result = await this.client.get(key);
    if (!result) {
      return null;
    }
    let envelope = null;
    try {
      envelope = JSON.parse(result, tagCacheUtil.dateReviver);
    } catch (ignoreErr) {
      // Handled by validation below
    }
    if (!envelope) {
      throw Error('Bad envelope content');
    }
    if (!envelope.stored || !envelope.hasOwnProperty('item')) {
      throw Error('Incorrect envelope structure');
    }
    return envelope;
  }

  async set(idAndSegment, value, ttl) {
    if (!this.client) {
      throw Error('Connection not started');
    }
    const {key, tag} = tagCacheUtil.parseTagAndKey(idAndSegment.id, idAndSegment.segment, this.options.partition);
    const envelope = {
      item: value,
      stored: Date.now(),
      ttl,
    };
    const json = JSON.stringify(envelope, tagCacheUtil.dateReplacer);
    const ttlSec = Math.max(1, Math.floor(ttl / 1000));

    //console.error('set', tag, key);

    const multi = await this.client.multi();
    if (tag) {
      multi.sadd(tag, key);
    }
    multi.set(key, json, 'ex', ttlSec);
    await multi.exec();
  }

  async drop(idAndSegment) {
    if (!this.client) {
      throw Error('Connection not started');
    }
    const {key, tag} = tagCacheUtil.parseTagAndKey(idAndSegment.id, idAndSegment.segment, this.options.partition);
    //console.error('drop', idAndSegment.id, tag, key);

    const pipeline = await this.client.pipeline();
    if (tag) {
      const keys = await this.client.smembers(tag);
      for (let i = 0; i < keys.length; i++) {
        pipeline.del(keys[i]);
      }
      pipeline.del(tag);
    }
    pipeline.del(key);
    await pipeline.exec();
  }
}

module.exports = CatboxRedisTagCache;