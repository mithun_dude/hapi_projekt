// import * as Boom from '@hapi/boom';
// import * as firebaseAdmin from 'firebase-admin';
// import util from '../util';
// import { Server } from '@hapi/hapi';



// const MERGE_OPTION = { merge: true };

// let app: firebaseAdmin.app.App

// const defaults = {
//   cache: {
//     cache: 'default',
//     segment: 'db',
//     expiresIn: 10 * 1000,
//   },
// };

// class FirebaseFb {

//   created: Date;
//   options: any;
//   stats: { reads: number; writes: number; deletes: number; };
//   cache: any;
//   app: firebaseAdmin.app.App;
//   logger!: {
//     debug: {
//       (message?: any, ...optionalParams: any[]): void;
//       (message?: any, ...optionalParams: any[]): void;
//     };
//     info: {
//       (message?: any, ...optionalParams: any[]): void;
//       (message?: any, ...optionalParams: any[]): void;
//     };
//     warn: {
//       (message?: any, ...optionalParams: any[]): void;
//       (message?: any, ...optionalParams: any[]): void;
//     };
//     error: {
//       (message?: any, ...optionalParams: any[]): void;
//       (message?: any, ...optionalParams: any[]): void;
//     }
//   };


//   constructor(server: Server, options: any) {
//     if (server) {
//       // this.logger = server.logger().child({ module: 'FirebaseDb' });
//     } else {
//       this.logger = { debug: console.error, info: console.error, warn: console.error, error: console.error };
//     }
//     //its any todo
//     this.options = Object.assign({}, defaults, options);
//     this.created = new Date();
//     this.stats = { reads: 0, writes: 0, deletes: 0 };
//     this.cache = server.cache(this.options.cache);

//     if (app) {
//       try {
//         let credential: firebaseAdmin.credential.Credential;
//         if (this.options.firebaseProjectId && this.options.firebaseClientEmail && this.options.firebasePrivateKey) {
//           this.logger.info('Initializing Firebase App %s, %s using private key', this.options.firebaseProjectId, this.options.firebaseUrl);
//           credential = firebaseAdmin.credential.cert({
//             projectId: this.options.firebaseProjectId,
//             clientEmail: this.options.firebaseClientEmail,
//             privateKey: this.options.firebasePrivateKey.replace(/\\n/gi, '\n')
//           });
//           app = firebaseAdmin.initializeApp({
//             credential: credential,
//             databaseURL: this.options.firebaseUrl,
//           });
//         } else if (this.options.firebaseProjectId) {
//           this.logger.info('Initializing Firebase App %s, %s using default credentials', this.options.firebaseProjectId, this.options.firebaseUrl);
//           credential = firebaseAdmin.credential.applicationDefault();
//           app = firebaseAdmin.initializeApp({
//             credential: credential,
//             databaseURL: this.options.firebaseUrl,
//           });
//         } else {
//           app = firebaseAdmin.initializeApp({
//             databaseURL: this.options.firebaseUrl,
//           });
//         }
//       }
//       catch (err) {
//         this.logger.error({ err }, 'Failed to initialize Firebase App: %s', err);
//         throw err;
//       }
//     }
//     this.app = app
//   }

//   //TODO
//   async retry(func:() => any, maxAttempts?: number, numRetry?: number):Promise<any> {
//     try {
//       return await func();
//     } catch (err) {
//       if (err.code === 2 || err.code === 10 || err.code === 13) { // 2: metadata error, 10: contention error, 13: internal error
//         maxAttempts = maxAttempts || 3;
//         numRetry = numRetry || 1;
//         if (numRetry <= maxAttempts) {
//           this.logger.warn({ err }, 'Firestore operation failed, retrying: %s', err);
//           await util.sleepExpBackoff(numRetry, 250);
//           return await this.retry(func, maxAttempts, numRetry + 1);
//         } else {
//           throw err;
//         }
//       } else {
//         throw err;
//       }
//     }
//   }
//   getDocCacheKey(docRef:any, includeTag?:any) {
//     if (includeTag) {
//       return `[${docRef.parent.path}]${docRef.path}`;
//     }
//     return docRef.path;
//   }

//   getQueryOrColCacheKey(queryOrColRef:any) {
//     if (queryOrColRef.path) {
//       return `[${queryOrColRef.path}]${queryOrColRef.path}`;
//     }
//     const queryOptions = queryOrColRef._queryOptions;
//     let path = '';
//     if (queryOptions.parentPath) {
//       const segments = queryOptions.parentPath.segments;
//       for (let i = 0; i < segments.length; i++) {
//         if (path) {
//           path += '/';
//         }
//         path += segments[i];
//       }
//     }
//     if (queryOptions.collectionId) {
//       if (path) {
//         path += '/';
//       }
//       path += queryOptions.collectionId;
//     }
//     let key = '';
//     key += '[';
//     key += path;
//     key += ']';
//     key += path;
//     const fieldFilters = queryOptions.fieldFilters;
//     if (fieldFilters) {
//       for (let i = 0; i < fieldFilters.length; i++) {
//         key += ':';
//         const filter = fieldFilters[i];
//         key += filter.field;
//         key += '-';
//         key += filter.op;
//         key += '-';
//         if (!filter.value) {
//           key += filter.value;
//         } else if (filter.value.path) {
//           key += filter.value.path;
//         } else if (filter.value.valueOf) {
//           key += filter.value.valueOf();
//         } else {
//           key += filter.value;
//         }
//       }
//     }
//     const fieldOrders = queryOptions.fieldOrders;
//     if (fieldOrders) {
//       for (let i = 0; i < fieldOrders.length; i++) {
//         key += ':';
//         key += fieldOrders[i].field;
//         key += '-';
//         key += fieldOrders[i].direction;
//       }
//     }
//     if (queryOptions.startAt || queryOptions.endAt) {
//       key += ':';
//       if (queryOptions.startAt) {
//         if (queryOptions.startAt.before) {
//           key += '<';
//         }
//         key += queryOptions.startAt.values.map((v:any) => v.path).join('&');
//       }
//       if (queryOptions.endAt) {
//         if (queryOptions.startAt) {
//           key += '-';
//         }
//         if (queryOptions.endAt.before) {
//           key += '<';
//         }
//         key += queryOptions.endAt.values.map((v:any) => v.path).join('&');
//       }
//     }
//     if (queryOptions.limit !== undefined || queryOptions.offset !== undefined) {
//       key += ':';
//       key += queryOptions.limit || '';
//       key += '-';
//       key += queryOptions.offset || '';
//     }
//     if (queryOptions.projection) {
//       const fields = queryOptions.projection.fields;
//       for (let i = 0; i < fields.length; i++) {
//         key += ':';
//         key += fields[i].fieldPath;
//       }
//     }
//     return key;
//   }

//   colRef(colPath:string) {
//     return this.app.firestore().collection(colPath);
//   }

//   docRef(docPath:string) {
//     return this.app.firestore().doc(docPath);
//   }

//   newDocRef(colPath:string) {
//     return this.app.firestore().collection(colPath).doc();
//   }

//   query(colPath:string) {
//     return this.colRef(colPath);
//   }

//   async fetchOne(docPath:string) {
//     const docRef:any = this.docRef(docPath);
//     const cacheKey:any = this.getDocCacheKey(docRef);
//     try {
//       const cacheValue = await this.cache.get(cacheKey);
//       if (cacheValue) {
//         return cacheValue;
//       }
//     } catch (err) {
//       this.logger.warn({ err }, 'Failed to get %s from cache', cacheKey);
//     }

//     const snap = await this.retry(() => docRef.get());
//     const data = snap.data();
//     if (!data) {
//       return null;
//     }
//     this.convertTimestamps(data);
//     data.id = snap.id;
//     this.stats.reads++;

//     this.cache.set(cacheKey, data).catch(err => this.logger.warn({ err }, 'Failed to set %s to cache', cacheKey));
//     return data;
//   }

//   async fetch(queryOrColRef) {
//     if (typeof queryOrColRef === 'string') {
//       queryOrColRef = this.colRef(queryOrColRef);
//     }
//     const cacheKey = this.getQueryOrColCacheKey(queryOrColRef);
//     try {
//       const cacheValue = await this.cache.get(cacheKey);
//       if (cacheValue) {
//         return cacheValue;
//       }
//     } catch (err) {
//       this.logger.warn({ err }, 'Failed to get %s from cache', cacheKey);
//     }

//     const snap = await this.retry(() => queryOrColRef.get());
//     const result = [];
//     snap.forEach(docSnap => {
//       const data = docSnap.data();
//       data.id = docSnap.id;
//       this.convertTimestamps(data);
//       result.push(data);
//       this.stats.reads++;
//     });
//     this.cache.set(cacheKey, result).catch(err => this.logger.warn({ err }, 'Failed to set %s to cache', cacheKey));
//     return result;
//   }

//   async create(colPath, data, skipTagCacheDrop) {
//     const docRef = await this.retry(() => this.colRef(colPath).add(this.cleanUpObject(data)));

//     const cacheKey = this.getDocCacheKey(docRef, !skipTagCacheDrop);
//     this.cache.drop(cacheKey).catch(err => this.logger.warn({ err }, 'Failed to drop %s from cache', cacheKey));

//     const snap = await this.retry(() => docRef.get());
//     data = snap.data();
//     this.convertTimestamps(data);
//     data.id = snap.id;
//     this.stats.writes++;
//     return data;
//   }

//   async update(docPath, data, skipTagCacheDrop) {
//     const docRef = this.docRef(docPath);

//     const cacheKey = this.getDocCacheKey(docRef, !skipTagCacheDrop);
//     this.cache.drop(cacheKey).catch(err => this.logger.warn({ err }, 'Failed to drop %s from cache', cacheKey));

//     await this.retry(() => docRef.set(this.cleanUpObject(data, true), MERGE_OPTION));
//     this.stats.writes++;
//     return data;
//   }

//   async set(docPath, data, skipTagCacheDrop) {
//     const docRef = this.docRef(docPath);

//     const cacheKey = this.getDocCacheKey(docRef, !skipTagCacheDrop);
//     this.cache.drop(cacheKey).catch(err => this.logger.warn({ err }, 'Failed to drop %s from cache', cacheKey));

//     await this.retry(() => docRef.set(this.cleanUpObject(data, true)));
//     this.stats.writes++;
//     return data;
//   }

//   async remove(docPath, subCols, skipTagCacheDrop) {
//     const docRef = this.docRef(docPath);
//     if (subCols === true) {
//       const ops = [{ type: 'delete', ref: docRef.path }];
//       const colRefs = await docRef.listCollections();
//       for (let i = 0; i < colRefs.length; i++) {
//         const subColDocRefs = await colRefs[i].listDocuments();
//         for (let j = 0; j < subColDocRefs.length; j++) {
//           ops.push({ type: 'delete', ref: subColDocRefs[j].path });
//         }
//       }
//       await this.batch(ops);
//     } else {
//       const cacheKey = this.getDocCacheKey(docRef, !skipTagCacheDrop);
//       this.cache.drop(cacheKey).catch(err => this.logger.warn({ err }, 'Failed to drop %s from cache', cacheKey));

//       await this.retry(() => docRef.delete());
//       this.stats.deletes++;
//     }
//   }

//   async removeCol(colPath, subCols) {
//     const docRefs = await this.colRef(colPath).listDocuments();
//     const ops = [];
//     for (let i = 0; i < docRefs.length; i++) {
//       ops.push({ type: 'delete', ref: docRefs[i].path });
//       if (subCols === true) {
//         const colRefs = await docRefs[i].listCollections();
//         for (let j = 0; j < colRefs.length; j++) {
//           const subColDocRefs = await colRefs[j].listDocuments();
//           for (let k = 0; k < subColDocRefs.length; k++) {
//             ops.push({ type: 'delete', ref: subColDocRefs[k].path });
//           }
//         }
//       }
//     }
//     await this.batch(ops);
//   }

//   async getDocRefs(ref, result) {
//     if (!result) {
//       result = [];
//     }
//     if (ref.listDocuments) {
//       const docRefs = await this.retry(() => ref.listDocuments());
//       for (let j = 0; j < docRefs.length; j++) {
//         await this.getDocRefs(docRefs[j], result);
//       }
//     } else {
//       result.push(ref);
//       const colRefs = await this.retry(() => ref.listCollections());
//       for (let i = 0; i < colRefs.length; i++) {
//         await this.getDocRefs(colRefs[i], result);
//       }
//     }
//     return result;
//   }

//   async removeRecursive(docPath) {
//     await this.batch((await this.getDocRefs(this.ref(docPath))).map(r => { return { type: 'delete', ref: r.path }; }));
//   }

//   async batch(operations, skipTagCacheDrop) {
//     if (!operations) {
//       throw new Error('Invalid operations: ' + operations);
//     }
//     let batch = this.app.firestore().batch();
//     let b = 0;
//     for (let i = 0; i < operations.length; i++) {
//       const op = operations[i];
//       const docRef = this.docRef(op.ref);
//       switch (op.type) {
//         case 'create':
//           batch.create(docRef, this.cleanUpObject(op.data));
//           this.stats.writes++;
//           break;
//         case 'update':
//           batch.set(docRef, this.cleanUpObject(op.data, true), MERGE_OPTION);
//           this.stats.writes++;
//           break;
//         case 'set':
//           batch.set(docRef, this.cleanUpObject(op.data, true));
//           this.stats.writes++;
//           break;
//         case 'delete':
//           batch.delete(docRef);
//           this.stats.deletes++;
//           break;
//       }
//       b++;

//       const cacheKey = this.getDocCacheKey(docRef, !skipTagCacheDrop);
//       this.cache.drop(cacheKey).catch(err => this.logger.warn({ err }, 'Failed to drop %s from cache', cacheKey));

//       if (b >= 500) {
//         await this.retry(() => batch.commit());
//         batch = this.app.firestore().batch();
//         b = 0;
//       }
//     }
//     if (b > 0) {
//       await this.retry(() => batch.commit());
//     }
//   }

//   async transaction(updateFunction, maxAttempts, affectedRefs) {
//     this.stats.writes += 3;
//     const result = await this.retry(() => this.app.firestore().runTransaction(updateFunction, { maxAttempts: maxAttempts || 5 }));
//     if (affectedRefs && affectedRefs.length > 0) {
//       await Promise.all(affectedRefs.map(r => this.cache.drop(this.getDocCacheKey(r, true)).catch(err => this.logger.warn({ err }, 'Failed to drop %s from cache', this.getDocCacheKey(r, true)))));
//     }
//     return result;
//   }

//   async increment(docPath, attribute, value) {
//     const docRef = this.docRef(docPath);

//     const cacheKey = this.getDocCacheKey(docRef, true);
//     await Promise.all([
//       this.cache.drop(cacheKey).catch(err => this.logger.warn({ err }, 'Failed to drop %s from cache', cacheKey)),
//       this.retry(() => docRef.update(attribute, this.incrementValue(value))),
//     ]);
//   }

//   async tryLock(docPath, timeout) {
//     timeout = timeout || 5000;
//     this.stats.reads++;
//     this.stats.writes++;
//     const docRef = this.docRef(docPath);
//     const start = new Date();
//     let result = false;
//     do {
//       result = await this.retry(() => this.app.firestore().runTransaction(async tx => {
//         const doc = await tx.get(docRef);
//         if (doc.exists) {
//           return false;
//         }
//         await tx.set(docRef, { created: start });
//         return true;
//       }, { maxAttempts: 5 }));
//       if (result) {
//         return true;
//       }
//       await new Promise(resolve => setTimeout(resolve, 500));
//     } while (Date.now() - start.valueOf() < timeout);
//     return false;
//   }

//   async releaseLock(docPath) {
//     this.stats.deletes++;
//     const docRef = this.docRef(docPath);
//     await this.retry(() => this.app.firestore().runTransaction(async tx => {
//       await tx.delete(docRef);
//     }, { maxAttempts: 5 }));
//   }

//   listen(queryOrRef, listenFunction, errorFunction) {
//     return queryOrRef.onSnapshot(listenFunction, errorFunction || (err => this.logger.error(err, 'Listener threw exception')));
//   }

//   async createUser(email, password, displayName, phoneNumber, photoUrl) {
//     try {
//       const user = await this.app.auth().createUser({
//         email: email,
//         emailVerified: true,
//         password: password,
//         phoneNumber: phoneNumber,
//         displayName: displayName,
//         photoURL: photoUrl,
//         disabled: false
//       });
//       return user;
//     } catch (err) {
//       const code = err.errorInfo && err.errorInfo.code;
//       switch (code) {
//         case 'auth/invalid-phone-number':
//         case 'auth/invalid-password':
//         case 'auth/invalid-photo-url':
//         case 'auth/invalid-display-name':
//         case 'auth/invalid-email-verified':
//         case 'auth/invalid-email':
//         case 'auth/email-already-exists':
//         case 'auth/phone-number-already-exists':
//           throw Boom.badRequest(err.message, { email, displayName, phoneNumber, photoUrl });
//         default:
//           throw err;
//       }
//     }
//   }

//   async authUser(accessToken) {
//     return this.app.auth().verifyIdToken(accessToken).catch(() => null);
//   }

//   async getOrCreateUser(email, password, displayName, phoneNumber, photoUrl) {
//     let user = await this.getUserByEmail(email);
//     if (!user) {
//       user = await this.createUser(email, password, displayName, phoneNumber, photoUrl);
//     }
//     return user;
//   }

//   async getUserByEmail(email) {
//     try {
//       return await this.app.auth().getUserByEmail(email);
//     } catch (err) {
//       if (err.errorInfo && err.errorInfo.code == 'auth/user-not-found') {
//         return null;
//       }
//       throw err;
//     }
//   }

//   async updateUser(uid, data) {
//     try {
//       await this.app.auth().updateUser(uid, data);
//     } catch (err) {
//       const code = err.errorInfo && err.errorInfo.code;
//       switch (code) {
//         case 'auth/invalid-phone-number':
//         case 'auth/invalid-password':
//         case 'auth/invalid-photo-url':
//         case 'auth/invalid-display-name':
//         case 'auth/invalid-email-verified':
//         case 'auth/invalid-email':
//         case 'auth/email-already-exists':
//         case 'auth/phone-number-already-exists':
//           delete data.password;
//           throw Boom.badRequest(err.message, data);
//         default:
//           throw err;
//       }
//     }
//     return await this.getUser(uid);
//   }

//   async getUser(uid) {
//     try {
//       return await this.app.auth().getUser(uid);
//     } catch (err) {
//       if (err.errorInfo && err.errorInfo.code == 'auth/user-not-found') {
//         return null;
//       }
//       throw err;
//     }
//   }

//   async removeUser(uid) {
//     await this.app.auth().deleteUser(uid);
//   }

//   async removeByEmail(email) {
//     const user = await this.getUserByEmail(email);
//     if (user) {
//       await this.removeUser(user.uid);
//     }
//   }

//   convertArrayTimestamps(arr) {
//     for (let i = 0; i < arr.length; i++) {
//       const value = arr[i];
//       if (value && value._seconds) {
//         arr[i] = new Date(value._seconds * 1e3 + Math.round((value._nanoseconds || 0) / 1e6));
//       } else if (value && value.constructor === Object) {
//         this.convertTimestamps(value);
//       } else if (value && value.constructor === Array) {
//         this.convertArrayTimestamps(value);
//       }
//     }
//     return arr;
//   }

//   convertTimestamps(data) {
//     for (let key in data) {
//       const value = data[key];
//       if (value && value._seconds) {
//         data[key] = new Date(value._seconds * 1e3 + Math.round((value._nanoseconds || 0) / 1e6));
//       } else if (value && value.constructor === Object) {
//         this.convertTimestamps(value);
//       } else if (value && value.constructor === Array) {
//         this.convertArrayTimestamps(value);
//       }
//     }
//     return data;
//   }

//   cleanUpArray(arr, isUpdate) {
//     const result = [];
//     for (let i = 0; i < arr.length; i++) {
//       const value = arr[i];
//       if (value === undefined) {
//         continue;
//       } else if (value === null) {
//         result.push(null);
//       } else if (value.constructor === Object) {
//         result.push(this.cleanUpObject(value, isUpdate));
//       } else if (value.constructor === Array) {
//         result.push(this.cleanUpArray(value, isUpdate));
//       } else {
//         result.push(value);
//       }
//     }
//     return result;
//   }

//   cleanUpObject(obj, isUpdate) {
//     const result = {};
//     for (let key in obj) {
//       const value = obj[key];
//       if (key === 'id') {
//         continue;
//       } else if (value === undefined) {
//         if (!isUpdate) {
//           continue;
//         }
//         result[key] = firebaseAdmin.firestore.FieldValue.delete();
//       } else if (value === null) {
//         result[key] = null;
//       } else if (value.constructor === Object) {
//         result[key] = this.cleanUpObject(value, isUpdate);
//       } else if (value.constructor === Array) {
//         result[key] = this.cleanUpArray(value, isUpdate);
//       } else {
//         result[key] = value;
//       }
//     }
//     return result;
//   }

//   timestampNow() {
//     return firebaseAdmin.firestore.Timestamp.now();
//   }

//   timestampToDate(timestamp) {
//     return timestamp.toDate();
//   }

//   dateToTimestamp(date) {
//     return firebaseAdmin.firestore.Timestamp.fromDate(date);
//   }

//   millisToTimestamp(millis) {
//     return firebaseAdmin.firestore.Timestamp.fromMillis(millis);
//   }

//   queryPlaceholderDocId() {
//     return firebaseAdmin.firestore.FieldPath.documentId();
//   }

//   incrementValue(value) {
//     return firebaseAdmin.firestore.FieldValue.increment(value);
//   }

//   arrayUnionValue(value) {
//     return firebaseAdmin.firestore.FieldValue.arrayUnion(value);
//   }

//   arrayRemoveValue(value) {
//     return firebaseAdmin.firestore.FieldValue.arrayRemove(value);
//   }

//   cleanKey(key) {
//     return key.replace(/[\s.#$/[\]]/g, '_');
//   }

//   clearStats() {
//     for (let k in this.stats) {
//       this.stats[k] = 0;
//     }
//   }

// }