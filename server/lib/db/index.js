'use strict';

const FirebaseDb = require('./firebase');

module.exports = {
  name: 'db',
  register: async function(server, options) {
    server.app.db = new FirebaseDb(server, options);
  },
  once: true,
};