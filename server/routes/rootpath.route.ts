import { ServerRoute } from "@hapi/hapi"

export const rootpath: ServerRoute = {
  path: '/',
  method: 'GET',
  handler: function (request, handler) {
    return handler.response({ statusCode: 200, message: 'Ok' }).code(200)
  },
}