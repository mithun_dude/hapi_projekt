import { ServerRoute } from "@hapi/hapi"

export const readiness_check: ServerRoute = {
  method: 'GET',
  path: '/readiness_check',
  handler: function (_request, _handler) {
    return { statusCode: 200, message: 'Ok' }
  }
}