import { ServerRoute } from "@hapi/hapi"

export const liveness_check: ServerRoute = {
  method: 'GET',
  path: '/liveness_check',
  handler: function (request, handler) {
    return { statusCode: 200, message: 'Ok' }
  }
}