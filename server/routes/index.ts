

import { ServerRoute, Server} from '@hapi/hapi'

import { rootpath } from './rootpath.route'

import { readiness_check } from './readiness_check.route'
import { liveness_check } from './liveness_check.route'


const test: ServerRoute = {
  path: '/test/{msg?}', //if ? is not provided the server will return a 404 error if msg is not provided
  method: 'GET',
  handler: function (request, handler) {
    const msg = request.params.msg ? request.params.msg : 'Nope'
    // if (msg === 'time') return handler.response({ statusCode: 200, message: `Timestamp ${server.methods[msg]()}` }).code(200)
    // else return handler.response({ statusCode: 200, message: `Test ${msg}` }).code(200)
    return handler.response({ statusCode: 200, message: `Test ${msg}` }).code(200)
  }

}
function initRoutes(server: Server) { 
  server.route(rootpath);
  server.route(test);
  server.route(liveness_check);
  server.route(readiness_check);
}
export default initRoutes

