import { ServerMethodConfigurationObject } from '@hapi/hapi'


function timeStampFunc(){
  return Date.now()
}

export const timeStamp:ServerMethodConfigurationObject = {
  name:'time',
  method:timeStampFunc,
  options:{}  

}