export interface metadata {
  processing_instruction:string,
  generation_timestamp:number,
  total_shards:number,
  //below props only req if total_shards > 1
  shard_number?:number, //starts from 0
  nonce?:number //a unique id across all shards of same file

}