/** 
 * A Generic type representation of the plugin register object.
  * * name: string;
  * * version?: string;
  * * register(a: S, b: T): Promise<U>;
*/

interface pluginType<S, T, U> {
  name: string;
  version?: string;
  register(server: S, options: T): Promise<U>;
  once?: boolean
}

export default pluginType