
interface coordinates {
  latitude: number,  //double
  longitude: number, //double
}


export interface localized_value {
  locale:string //"eg: JP"
  value:string // "eg:- Konichiva Sikai(in japanese)"
}

export interface Text{
  value:string,   //eg:-"Hello world"
  localized_value:localized_value[]
}

export interface terms {
  url?: string,
  text: string,
  localized_text:Text
}

export interface PostalAddress {
  country: string, //Eg:-IN
  locality: string, //Eg:-Trivandrum
  region?: string, //(optional) Eg:-CA, where the locality is region specific
  postal_code: string,
  street_address: string, //Eg:-'1200 Wash road'
}

export interface geo extends coordinates {
  address?: PostalAddress 
  unstructured_address?: string
}

export interface matching_hints {
  place_id: string //place id in google maps https://developers.google.com/places/place-id 
}

export interface Value {
  value_id:string, 
  value_name:string //type of haircuts
}


export interface service_attribute {
  attribute_id:string, 
  attribute_name:string //eg:- haircut is a service
  value:Value[]
}

export interface merchant {
  merchant_id: string,
  name: string,
  geo: geo,
  category: string,
  telephone?: string, //+countrycode should be given
  url?: string,    
  terms?: terms,
  brand_id?: string,
  matching_hints?: matching_hints,
  service_attribute?:service_attribute[]
}

